﻿using Quartz;
using Quartz.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuartzSchedule
{
   public class LogProvider : ILogProvider
   {
      public Logger GetLogger(string name)
      {
         return (level, func, exception, parameters) =>
         {
            if (level >= LogLevel.Info && func != null)
            {
               Console.WriteLine("From Logger [" + DateTime.Now.ToLongTimeString() + "] [" + level + "] " + func(), parameters);
            }
            return true;
         };
      }

      public IDisposable OpenNestedContext(string message)
      {
         throw new NotImplementedException();
      }

      public IDisposable OpenMappedContext(string key, string value)
      {
         throw new NotImplementedException();
      }
   }



   public class Clander : ICalendar
   {
      public string Description { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
      public ICalendar CalendarBase { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

      public ICalendar Clone()
      {
         throw new NotImplementedException();
      }

      public DateTimeOffset GetNextIncludedTimeUtc(DateTimeOffset timeUtc)
      {
         throw new NotImplementedException();
      }

      public bool IsTimeIncluded(DateTimeOffset timeUtc)
      {
         throw new NotImplementedException();
      }
   }
}
