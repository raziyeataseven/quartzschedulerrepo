﻿using Newtonsoft.Json;
using Quartz;
using Quartz.Impl;
using QuartzSchedule;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OTI.TaskScheduler.Scheduler
{
   class Program
   {
      static void Main(string[] args)
      {
         string jsonData;
         using (StreamReader r = new StreamReader(@"C:\source\\external_jobgroup_info.json"))
         {
            jsonData = r.ReadToEnd();
         }

         InitJobAsync(jsonData).GetAwaiter().GetResult();
         Console.ReadKey();
      }

      internal static NameValueCollection props = new NameValueCollection
                {
                    { "quartz.serializer.type", "binary" },
                    { "quartz.threadPool.threadCount", "10" }
                };

      public static async Task InitJobAsync(string jsonData)
      {
         try
         {
            //read information of job
            var list = JsonConvert.DeserializeObject<List<JobGroup>>(jsonData);

            //init schedular and start
            StdSchedulerFactory factory = new StdSchedulerFactory(props);
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();

            //init jobs and its triggers
            var dictionary = new Dictionary<IJobDetail, List<ITrigger>>();
            foreach (var group in list)
            {
               foreach (var j in group.Jobs)
               {
                  //yalnızca bir tane Job class olacak çünkü her iş için aynı Execute() methodu çalıştırılacak.
                  //Job'un işi, taskını ve diğer bilgileri rabbitmq'a mesaj atmak olacak.

                  CronScheduleBuilder cronExp = CronExpressionHelper.GetCronExpression(group.Trigger);

                  //Type type = Assembly.Load("QuartzSample").GetTypes().First(t => t.Name == j.name);
                  IJobDetail jobDetail = JobBuilder.Create<DumbJob>()
                     .WithIdentity(j.Name, group.Name)
                     .UsingJobData("task", j.Task)
                     .Build();



                  ITrigger trigger1 = TriggerBuilder.Create()
                                       .WithIdentity(group.Trigger.Name + j.Name, group.Name)
                                       .ForJob(jobDetail)
                                       .StartAt(group.Trigger.StartNow ? DateTime.Now : (DateTimeOffset)group.Trigger.StartAt)
                                                              //.StartNow()
                                                              .WithSchedule(cronExp)
                                       //.WithSimpleSchedule(x => x
                                       //    .WithIntervalInSeconds(5)
                                       //    .WithMisfireHandlingInstructionFireNow()
                                       //    .RepeatForever())
                                       .WithDescription(group.Trigger.Name)
                                       .Build();



                  var triggerList = new List<ITrigger>();
                  triggerList.Add(trigger1);
                  dictionary.Add(jobDetail, triggerList);
               }
            }

            IReadOnlyDictionary<IJobDetail, IReadOnlyCollection<ITrigger>> keyValuePairs =
               dictionary.ToDictionary(kv => kv.Key, kv => kv.Value as IReadOnlyCollection<ITrigger>);
            await scheduler.ScheduleJobs(keyValuePairs, false);


            Console.Write("change the job runtime");
            // Tell quartz to schedule the job using our trigger
            //await scheduler.ScheduleJob(job, trigger);

            // some sleep to show what's happening
            //await Task.Delay(TimeSpan.FromSeconds(10));

            // and last shut down the scheduler when you are ready to close your program
            //await scheduler.Shutdown();

         }
         catch (Exception ex)
         {
            throw ex;
         }
      }


      public static async Task<bool> DeleteJob(string jobName, string groupName)
      {
         StdSchedulerFactory factory = new StdSchedulerFactory(props);
         IScheduler scheduler = await factory.GetScheduler();

         var jobKey = new JobKey(jobName, groupName);
         return await scheduler.DeleteJob(jobKey);
      }


      public static async Task StopJob(string jobName, string groupName)
      {
         StdSchedulerFactory factory = new StdSchedulerFactory(props);
         IScheduler scheduler = await factory.GetScheduler();

         var jobKey = new JobKey(jobName, groupName);
         await scheduler.PauseJob(jobKey);
      }

      public static async Task<bool> UnScheduleTrigger(string triggerName, string groupName)
      {
         StdSchedulerFactory factory = new StdSchedulerFactory(props);
         IScheduler scheduler = await factory.GetScheduler();

         var triggerKey = new TriggerKey(triggerName, groupName);
         return await scheduler.UnscheduleJob(triggerKey);
      }

      public static async Task<bool> Interrupt(string jobName, string groupName)
      {
         StdSchedulerFactory factory = new StdSchedulerFactory(props);
         IScheduler scheduler = await factory.GetScheduler();

         var jobKey = new JobKey(jobName, groupName);
         return await scheduler.Interrupt(jobKey);
      }

      public static async Task ReScheduleJob(string jobName, string groupName, ITrigger trigger)
      {
         StdSchedulerFactory factory = new StdSchedulerFactory(props);
         IScheduler scheduler = await factory.GetScheduler();

         //IJobDetail updatedJobDetail = JobBuilder.Create<DumbJob>()
         //   .WithIdentity(jobName, groupName)
         //   .Build();
         //await scheduler.AddJob(updatedJobDetail, true, true); // 2nd parameter true means updating the existing job with the updated one.
         
         //ITrigger have job info
         await scheduler.RescheduleJob(new TriggerKey("oldTrigger","groupName"), trigger);
      }

      public class DumbJob : IJob
      {
         public async Task Execute(IJobExecutionContext context)
         {
            try
            {
               JobKey key = context.JobDetail.Key;
               var executionTime = DateTime.Now.ToLongTimeString();
               string jobTask = context.JobDetail.JobDataMap.GetString("task");

               RabbitHelper rabbitHelper = new RabbitHelper("localhost", "guest", "guest", "scheduler");
               //send message to rabbitmq
               rabbitHelper.SendMessage("Instance " + key + " of DumbJob's task: " + jobTask);

               //send message console.log
               await Console.Error.WriteLineAsync("Instance " + key + " of DumbJob's task: " + jobTask);
            }
            catch (Exception ex)
            {

               throw ex;
            }
         }
      }
   }
}
