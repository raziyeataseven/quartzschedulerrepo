﻿using Quartz;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QuartzScheduler
{
   public class JobListener : IJobListener
   {
      private IJobDetail _jobDetail;
      public JobListener(IJobDetail jobDetail)
      {
         _jobDetail = jobDetail;
      }

      public string Name => throw new NotImplementedException();

      public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken))
      {
         throw new NotImplementedException();
      }

      public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken))
      {
         Console.WriteLine("Job {0} in group {1} is about to be executed", context.JobDetail.Key.Name, context.JobDetail.Key.Group);
         return Task.FromResult(context);
                
      }

      public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default(CancellationToken))
      {
         Console.WriteLine("Job {0} in group {1} was executed", context.JobDetail.Key.Name, context.JobDetail.Key.Group);

         // only run second job if first job was executed successfully
         if (jobException == null)
         {
            // fetching name of the job to be executed sequentially
            string nextJobName = Convert.ToString(context.MergedJobDataMap.GetString("Job2"));

            if (!string.IsNullOrEmpty(nextJobName))
            {
               Console.WriteLine("Next job to be executed :" + nextJobName);
               IJobDetail job = null;
               
               // define a job and tie it to our JobTwo class
               if (nextJobName == "Job2") // similarly we can write/handle cases for other jobs as well
               {
                  job = JobBuilder.Create<DumbJob>()
                          .WithIdentity("Job2", "JobTwoGroup")
                          .Build();
               }

               // create a trigger to run the job now 
               ITrigger trigger = TriggerBuilder.Create()
                   .WithIdentity("SimpleTrigger", "SimpleTriggerGroup")
                   .StartNow()
                   .Build();

               // finally, schedule the job
               if (job != null)
                  context.Scheduler.ScheduleJob(job, trigger);
            }
            else
            {
               Console.WriteLine("No job to be executed sequentially");
            }
         }
         else
         {
            Console.WriteLine("An exception occured while executing job: {0} in group {1} with following details : {2}",
                context.JobDetail.Key.Name, context.JobDetail.Key.Group, jobException.Message);
         }
         return Task.FromResult(context);
      }
   }
}
