﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuartzSchedule
{
   public class JobModels
   {
      public class JobGroup
      {
         public int ID { get; set; }
         public string Name { get; set; }
         public List<Job> Jobs { get; set; }
         public Trigger Trigger { get; set; }
         public bool RunOnlyOneInstance { get; set; }
         public bool IsSync { get; set; }
         public string GroupAuthMail { get; set; }

      }
      public class Job
      {
         public int ID { get; set; }
         public string Name { get; set; }
         public string Task { get; set; }
         public string JobAuthMail { get; set; }
      }

        /// <summary>
        /// Quartz cron expression will be create according to Trigger class
        /// </summary>
      public class Trigger
      {
         public int ID { get; set; }
         public string Name { get; set; }

         //tells to trigger: job's execution time is now
         public bool StartNow { get; set; }

         public DateTimeOffset? StartAt { get; set; }
         public int? Year { get; set; }
         public List<Month> Months { get; set; }
         public int[] DaysofMonth { get; set; }
         public DayOfWeek[] DaysOfWeek { get; set; }
         public DateTimeOffset? Duration { get; set; }
         public DateTimeOffset[] TimeBetween { get; set; }
         public FrequencyType FrequencyType { get; set; }
         public TimeList TimeList { get; set; }
         public BetweenMonthsAndYears BetweenMonthsAndYears { get; set; }
      }

      public class TimeList
      {
         public int[] Seconds { get; set; }
         public int[] Hours { get; set; }
         public int[] Minutes { get; set; } 
      }

      public class BetweenMonthsAndYears
      {
         public int[] Months { get; set; }
         public int[] Years { get; set; }      
      }

      public enum Month
      {
         January = 1,
         February = 2,
         March = 3,
         April = 4,
         May = 5,
         June = 6,
         July = 7,
         August = 8,
         September = 9,
         October = 10,
         November = 11,
         December = 12
      }     

      public enum FrequencyType { yearly, monthly, weekly, daily, hourly, onlyOnce }

   }
}
