﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using static OTI.TaskScheduler.Scheduler.JobModels;

namespace OTI.TaskScheduler.Scheduler
{
   public class CronExpressionHelper
   {
      public static CronScheduleBuilder GetCronExpression(Trigger trigger)
      {
         try
         {
            FrequencyType frequencyType = (FrequencyType)Enum.ToObject(typeof(FrequencyType), trigger.FrequencyType);
            string exp = "", monthsExp = "*", dayExp = "*",
               hourExp = "*", minuteExp = "*", secondExp = "*", yearExp = "*";

            if (trigger.Year != null)
               yearExp = trigger.Year.ToString();

            else if (trigger.BetweenMonthsAndYears != null)
            {
               if (trigger.BetweenMonthsAndYears.Months != null && trigger.BetweenMonthsAndYears.Months.Length > 0)
               {
                  monthsExp = string.Empty;
                  monthsExp = trigger.BetweenMonthsAndYears.Months.First().ToString() + "-" + trigger.BetweenMonthsAndYears.Months.Last().ToString();
               }
               if (trigger.BetweenMonthsAndYears.Years != null && trigger.BetweenMonthsAndYears.Years.Length > 0)
               {
                  yearExp = string.Empty;
                  yearExp = trigger.BetweenMonthsAndYears.Years.First().ToString() + "-" + trigger.BetweenMonthsAndYears.Years.Last().ToString();
               }
            }

            if (trigger.Months != null && trigger.Months.Count > 0)
            {
               monthsExp = string.Empty;
               foreach (var item in trigger.Months)
               {
                  //for cron, pass the first 3 letter of month(JAN for JANUARY)
                  monthsExp += item.ToString().ToUpper(CultureInfo.CreateSpecificCulture("en-EN")).Substring(0, 3) + ",";
               }
               monthsExp = monthsExp.Remove(monthsExp.Length - 1, 1);
            }

            if (trigger.DaysofMonth != null && trigger.DaysofMonth.Length > 0)
            {
               dayExp = string.Empty;
               foreach (var item in trigger.DaysofMonth)
               {
                  dayExp += item.ToString() + ",";
               }
               dayExp = dayExp.Remove(dayExp.Length - 1, 1);
            }

            if (trigger.DaysOfWeek != null && trigger.DaysOfWeek.Length > 0)
            {
               dayExp = string.Empty;
               foreach (var item in trigger.DaysOfWeek)
               {
                  // for cron, pass first 3 letter of day(SUN for SUNDAY)
                  dayExp += item.ToString().ToUpper(CultureInfo.CreateSpecificCulture("en-EN")).Substring(0, 3) + ",";
               }
               dayExp = dayExp.Remove(dayExp.Length - 1);
            }

            if (trigger.TimeBetween != null && trigger.TimeBetween.Length == 2)
            {
               hourExp = string.Empty;
               minuteExp = string.Empty;
               secondExp = string.Empty;
               hourExp = trigger.TimeBetween.First().Hour + "-" + trigger.TimeBetween.Last().Hour;
               minuteExp = trigger.TimeBetween.First().Minute + "-" + trigger.TimeBetween.Last().Minute;

               if (trigger.TimeBetween.First().Second == 0 && trigger.TimeBetween.Last().Second == 0)
                  secondExp = "*";
               else
                  secondExp = trigger.TimeBetween.First().Second + "-" + trigger.TimeBetween.Last().Second;

            }
            else
            {
               if (trigger.TimeList != null)
               {

                  if (trigger.TimeList.Hours != null && trigger.TimeList.Hours.Length > 0)
                  {
                     hourExp = string.Empty;

                     foreach (var item in trigger.TimeList.Hours)
                     {
                        hourExp += item.ToString() + ",";
                     }
                     hourExp = hourExp.Remove(hourExp.Length - 1);
                  }

                  if (trigger.TimeList.Minutes != null && trigger.TimeList.Minutes.Length > 0)
                  {
                     minuteExp = string.Empty;
                     foreach (var item in trigger.TimeList.Minutes)
                     {
                        minuteExp += item.ToString() + ",";
                     }
                     minuteExp = minuteExp.Remove(minuteExp.Length - 1);
                  }

                  if (trigger.TimeList.Seconds != null && trigger.TimeList.Seconds.Length > 0)
                  {
                     secondExp = string.Empty;
                     foreach (var item in trigger.TimeList.Seconds)
                     {
                        secondExp += item.ToString() + ",";
                     }
                     secondExp = secondExp.Remove(secondExp.Length - 1);
                  }
               }

            }


            //exp = string.Format("{0} {1} {2} {3} {4} ? {5}", secondExp, minuteExp, hourExp, dayExp, monthsExp, yearExp);
            //var exp2 = string.Format("{0} {1} {2} ? {3} {4} {5}", secondExp, minuteExp, hourExp, dayExp, monthsExp, yearExp);

            switch (frequencyType)
            {
               case FrequencyType.monthly:
                  exp = string.Format("{0} {1} {2} {3} {4} ? {5}", secondExp, minuteExp, hourExp, dayExp, monthsExp, yearExp);
                  break;

               case FrequencyType.weekly:
                  exp = string.Format("{0} {1} {2} ? {3} {4} {5}", secondExp, minuteExp, hourExp, monthsExp, dayExp, yearExp);
                  break;

               case FrequencyType.daily:
                  exp = string.Format("{0} {1} {2} {3} {4} ?", secondExp, minuteExp, hourExp, dayExp, monthsExp);
                  break;

               case FrequencyType.onlyOnce:
                  exp = string.Format("0 {0} {1} {2} {3} ? {4}", minuteExp, hourExp, dayExp, monthsExp, yearExp);
                  break;

               default:
                  throw new NotImplementedException("Trigger couldn't initialize. Trigger name:" + trigger.Name);
            }


            return CronScheduleBuilder.CronSchedule(exp);

         }
         catch (Exception ex)
         {

            throw ex;
         }
      }
   }
}










//switch (frequencyType)
//{

//   case FrequencyType.monthly:
//      exp2 = string.Format("{0} {1} {2} {3} {4} ? *", secondExp, minuteExp, hourExp, dayExp, monthsExp);
//      csb = CronScheduleBuilder.CronSchedule(monthsExp);
//      break;

//   case FrequencyType.weekly:
//      exp2 = string.Format("{0} {1} {2} ? * {3} *", secondExp, minuteExp, hourExp, dayExp);
//      csb = CronScheduleBuilder.CronSchedule(exp);
//      //csb = CronScheduleBuilder.WeeklyOnDayAndHourAndMinute(DayOfWeek.Friday, trigger.Hour.Hour, trigger.Hour.Minute);
//      break;

//   case FrequencyType.daily:
//      exp2 = string.Format("{0} {1} {2} ? * * *", secondExp, minuteExp, hourExp);
//      csb = CronScheduleBuilder.CronSchedule(exp);
//      break;

//   case FrequencyType.onlyOnce:
//      exp2 = string.Format("0 {0} {1} {2} {3} ? *", minuteExp, hourExp, dayExp, monthsExp);
//      csb = CronScheduleBuilder.CronSchedule(exp);
//      break;

//   default:
//      throw new NotImplementedException("Trigger couldn't initialize. Trigger name:" + trigger.Name);
//}